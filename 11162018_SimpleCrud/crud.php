<?php include('database.php');
	
	// fetch the record to be updated
	if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		$edit_contact = true;
		$rec = mysqli_query($db, "SELECT * FROM contacts WHERE id=$id");
		$record = mysqli_fetch_array($rec);
		$name = $record['name'];
		$contactNo = $record['contactNo'];
		$id = $record['id'];
	}	

?>
<!DOCTYPE html>
<html>
<head>
	<title>CRUD: CReate, Update, Delete PHP MySQL</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>

	<?php if (isset($_SESSION['msg'])): ?>
		<div class="msg">
			<?php 
				echo $_SESSION['msg']; 
				unset($_SESSION['msg']);
			?>
		</div>
	<?php endif ?>
	
	<?php $results = mysqli_query($db, "SELECT * FROM contacts"); ?>
	
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Contact Number</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php while ($row = mysqli_fetch_array($results)) { ?>
		<tr>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['contactNo']; ?></td>
			<td>
				<a href="crud.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >Edit</a>

			</td>
			<td>
				<a href="database.php?del=<?php echo $row['id']; ?>" class="del_btn">Delete</a>
			</td>
		</tr>
	<?php } ?>
		</tbody>
	</table>
	
	<form method="post" action="database.php" >
	<input type="hidden" name="id" value="<?php echo $id; ?>">
		<div class="input-group">
			<label>Name</label>
			<input type="text" name="name" value="<?php echo $name; ?>">
		</div>
		<div class="input-group">
			<label>Contact Number</label>
			<input type="text" name="contactNo" value="<?php echo $contactNo; ?>">
		</div>
		<div class="input-group">
		
		<?php if ($edit_contact == true): ?>
			<button class="btn" type="submit" name="update" style ="background: #556B2f;">Update</button>
		<?php else: ?>
			<button class="btn" type="submit" name="save">Save</button>
		<?php endif ?>
			
			
		</div>
	</form>
</body>
</html>