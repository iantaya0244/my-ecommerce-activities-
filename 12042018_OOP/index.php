<?php
include_once("Student.Class.php");


$newStudent = new Student();


trace($newStudent);


$newStudent->setStudId(12345678);
$newStudent->setStudFirstName("Gabriel Ian");
$newStudent->setStudLastName("Babiera");
$newStudent->setStudSex("Male");
$newStudent->setStudYearLevel(4);
$newStudent->setStudCourse("BSIT");
$newStudent->setStudAge(100);
$newStudent->setStudBirthDate("01/02/2000");

echo "Student info:<br><br>";
echo "Student ID: " . $newStudent->getStudID() . "<br>";
echo "Name: " . $newStudent->getStudFirstName(). " " . $newStudent->getStudLastName()."<br>";
echo "Gender: " . $newStudent->getStudSex(). "<br>";
echo "Year Level: " . $newStudent->getStudYearLevel(). "<br>";
echo "Course: " . $newStudent->getStudCourse(). "<br>";
echo "Age: " . $newStudent->getStudAge(). "<br>";
echo "Birthdate: " . $newStudent->getStudBirthDate() . "<br>";

function trace($x) {
	echo "<pre>";
	print_r($x);
	echo "</pre>";
}

?>